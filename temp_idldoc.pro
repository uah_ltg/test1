; docformat = 'rst'
;+
;
; Just a routine that does nothing but provide somewhere to test `IDLDoc <http://https://github.com/mgalloy/idldoc>`
; 
; (It also serves as a pseudo-file template with compiler options and a standard error handler.)
;
; This also works for me as reference guide :-)
;
; :Examples:
;   All the cool things you can do with IDLDoc:
; 
;   You can provide external links::
;     `Phillip Bitzer <http://www.nsstc.uah.edu/ats/bitzer/index.html>
;   `Phillip Bitzer <http://www.nsstc.uah.edu/ats/bitzer/index.html>
; 
;   You can link to images::
;     .. image:: el_map_count_rev_2013.png
;   
;   .. image:: el_map_count_rev_2013.png
;   
;   You can embed SVG files too::
;     .. embed:: filename
;     
;   (Sorry, no example here.)
;     
;   I find LaTeX handy::
;     \frac{1}{\sqrt{2 \pi}} e^{-z^2}
;   
;   $$\frac{1}{\sqrt{2 \pi}} e^{-z^2}$$
;   
;   And with a little arm twisting, you can add snippets of HTML code::
;     This is .. html:: <del>good</del> <add>great</add>!
;   This is .. html:: <del>good</del> <add>great</add>!
;     
;   But what I think is really handy is to embed a YouTube video::
;      .. html:: <iframe width="420" height="315" src="http://www.youtube.com/embed/GBUJaT_YYs0?rel=0" frameborder="0" allowfullscreen></iframe>
;   
;   .. html:: <iframe width="420" height="315" src="http://www.youtube.com/embed/GBUJaT_YYs0?rel=0" frameborder="0" allowfullscreen></iframe>
;   
;   (Note that I have to add "http:" in the embed link YouTube provides.)
;   
;   I know, I know. You came here for some lightning videos:
;   
;   .. html:: <iframe width="420" height="315" src="http://www.youtube.com/embed/F9-RwX52llI?rel=0" frameborder="0" allowfullscreen></iframe>
;
;  HTML code is handled if the first character (after the double colon-space) is "<"
;  
; :Uses:
;   None
;
;-
;
FUNCTION temp_idldoc, parameter1, anotherParameter, key1=key1, key2=oKey
;+
; This function doesn't do anything meaningful. It's just here for testing IDLDoc.
; I suppose you could use for a template with which to start writing some routines.
;
;  :Returns:
;    Goobedlygook. Nothing of any importance. I'm sure the functions you write are awesome, though.
;    
;    If you had some fancy output, you could use HTML to format it:
;    
;     .. html:: <table><tr><td>-1</td><td>IF</td><td>UTC1</td><td align="center">&lt</td><td>UTC2</td></tr>
;     .. html:: <tr><td>0</td><td>IF</td><td>UTC1</td><td align="center">==</td><td>UTC2</td></tr>
;     .. html:: <tr><td>1</td><td>IF</td><td>UTC1</td><td align="center">&gt</td><td>UTC2</td></tr>
;     .. html:: </table>
;    
;  Note: The HTML code for this using the align attribute of the td tag. This won't work with HTML5 - you need
;  to use a style sheet.
;  
;    
;  :Params:
;    parameter1 : in, required, type=whatever
;      This first super-cool positional parameter for your routine.
;    anotherParameter : in, optional, type=whatever, default=11.0
;      Another super-cool parameter
;   
;   :Keywords:
;     key1 : in, optional, type = boolean, default=0B
;       Just a boolean keyword. Set it and I'll print `anotherParameter`.
;       (Hey! You can link to other parameters and keywords!)
;     key2 : out, optional, type=string, default="Pure Awesomeness"
;       For kicks and giggles, make an output keyword too.
;       Check out the code to see the way we should handle output keywords.
;    
;  :Uses:
;    None
;
;-

compile_opt idl2

;error catcher:
CATCH, theError
IF theError NE 0 THEN BEGIN
  CATCH, /cancel
  HELP, /LAST_MESSAGE, OUTPUT=errMsg
  FOR i=0, N_ELEMENTS(errMsg)-1 DO print, errMsg[i]
  RETURN, !VALUES.F_NAN
ENDIF

;check the parameters:
IF N_ELEMENTS(parameter1) EQ 0 THEN MESSAGE, "I need at _least_ one parameter!"

IF N_ELEMENTS(anotherParameter) EQ 0 THEN anotherParameter=11.0

;we check boolean keywords with KEYWORD_SET:
IF KEYWORD_SET(key1) THEN print, anotherParameter 

;but, we check output keywords with ARG_PRESENT...notice I use what's on the left hand side of the declaration:
IF ARG_PRESENT(oKey) THEN oKey = 'Pure Awesomeness' 

RETURN, 'goobedlygook' ;return the result of the function

END ;temp_idldoc;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;Main Level Program

;OK, say you're brand new to IDL. Here's how you can use this nonsensical function.

result = TEMP_IDLDOC() ;error!

result = TEMP_IDLDOC(4.0)
print, result ;told you it returned goobedlygook

result = TEMP_IDLDOC(4.0, /KEY1) ;I guess you could print, result, but it's the same

result = TEMP_IDLDOC(4.0, SIN(!PI), /KEY1) ;parameters can be function outputs

result = TEMP_IDLDOC(4.0, SIN(!PI), KEY2=outputKey)
print, outputKey

END
